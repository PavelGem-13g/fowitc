﻿using System.Collections;
using UnityEngine;

public class AnimationObject : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Rotate());
    }

    IEnumerator Rotate()
    {
        while (true)
        {
            transform.Rotate(new Vector3(0, 1, 0));
            yield return new WaitForEndOfFrame();
        }
    }
}
