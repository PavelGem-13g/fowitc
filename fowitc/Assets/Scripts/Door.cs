﻿using Assets.Scripts;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Door : MonoBehaviour
{
    public int id;
    public string startTime;
    public string endTime;
    public string description;

    public Text text;

    private void Start()
    {
        id = Convert.ToInt32(name);
    }
    public void UpdateInformation()
    {
        ScheduleTable scheduleTemp = SQL.instance.GetScheduleInformation(id);
        Debug.LogWarning(scheduleTemp.ToString());
        if (scheduleTemp == new ScheduleTable())
        {
            text.text = "Нет занятий";
        }
        else
        {
            startTime = scheduleTemp.startTime;
            endTime = scheduleTemp.endTime;
            description = scheduleTemp.description;
            text.text = id + " " + startTime + "-" + endTime + " " + description;
        }
    }
}
