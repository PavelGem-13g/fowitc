﻿using Assets.Scripts;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.Common;
using UnityEngine;

public class SQL : MonoBehaviour
{
    string host = "remotemysql.com";
    private string database = "JiQRuMfhFj";
    private string port = "3306";
    private string username = "JiQRuMfhFj";
    private string password = "J9MyW4oGCj";
    MySqlConnection connection;
    public static SQL instance;
    private void Awake()
    {
        instance = this;
        Connect();
        DontDestroyOnLoad(gameObject);
    }
    void Connect()
    {
        string connString =
        "Server=" + host + ";" +
        "Database=" + database + ";" +
        "port=" + port + ";" +
        "User Id=" + username + ";" +
        "password=" + password;
        connection = new MySqlConnection(connString);
    }
    public ScheduleTable GetScheduleInformation(int id)
    {
        List<ScheduleTable> results = new List<ScheduleTable>();
        connection.Open();
        MySqlCommand mySqlCommand = connection.CreateCommand();
        mySqlCommand.CommandText = $"SELECT * FROM schedule WHERE id = {id}";
        DbDataReader reader = mySqlCommand.ExecuteReader();
        if (reader.HasRows)
        {
            while (reader.Read())
            {
                results.Add(new ScheduleTable(reader.GetValue(reader.GetOrdinal("startTime")).ToString(),
                    reader.GetValue(reader.GetOrdinal("endTime")).ToString(),
                    reader.GetValue(reader.GetOrdinal("description")).ToString(),
                    Convert.ToInt32(reader.GetValue(reader.GetOrdinal("day")).ToString())));
            }
        }
        connection.Close();
        return ScheduleTable.FindRightEvent(results);

    }
    public List<string> GetDoorInformation(int number)
    {
        List<string> result = new List<string>();
        connection.Open();
        MySqlCommand mySqlCommand = connection.CreateCommand();
        mySqlCommand.CommandText = $"SELECT * FROM list WHERE number = {number}";
        DbDataReader reader = mySqlCommand.ExecuteReader();
        if (reader.HasRows)
        {
            while (reader.Read())
            {
                result.Add(reader.GetValue(reader.GetOrdinal("id")).ToString());
                result.Add(reader.GetValue(reader.GetOrdinal("number")).ToString());
            }
            connection.Close();
        }
        else
        {
            connection.Close();
            Command($"INSERT INTO list (number) VALUES ({number})");
            connection.Open();
            mySqlCommand.CommandText = $"SELECT * FROM list WHERE number = {number}";
            reader = mySqlCommand.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    result.Add(reader.GetValue(reader.GetOrdinal("id")).ToString());
                    result.Add(reader.GetValue(reader.GetOrdinal("number")).ToString());
                }
                connection.Close();
            }
        }
        return result;
    }

    void Command(string command)
    {
        connection.Open();
        try
        {
            using (MySqlCommand cmd = connection.CreateCommand())
            {
                cmd.CommandText = command;
                cmd.ExecuteNonQuery();
            }
        }
        catch (Exception e)
        {
            Debug.LogWarning("Sql " + e.Message);
        }
        connection.Close();
    }
}
