﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts
{
    public class ScheduleTable
    {
        public string startTime;
        public string endTime;
        public string description;
        public int day;

        public ScheduleTable()
        {

        }
        public ScheduleTable(string startTime, string endTime, string description, int day)
        {
            this.startTime = startTime;
            this.endTime = endTime;
            this.description = description;
            this.day = day;
        }
        public static ScheduleTable FindRightEvent(List<ScheduleTable> scheduleTables)
        {
            for (int i = 0; i < scheduleTables.Count; i++)
            {
                if (scheduleTables[i].day != DayTime.GetDay())
                {
                    scheduleTables.Remove(scheduleTables[i]);
                }
            }
            foreach (var item in scheduleTables)
            {
                if (DayTime.IsOneLess(item.startTime, DayTime.GetTime()) && DayTime.IsOneLess(DayTime.GetTime(), item.endTime))
                {
                    return item;
                }
            }
            return new ScheduleTable();
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return "st " + startTime +" ed " + endTime + " ds " +description + " d " + day;
        }
        public static bool operator ==(ScheduleTable one,ScheduleTable two) 
        {
            return one.startTime == two.startTime && one.endTime == two.endTime && one.description == two.description && one.day == two.day;
        }
        public static bool operator !=(ScheduleTable one, ScheduleTable two)
        {
            return !(one == two);
        }
    }
}
