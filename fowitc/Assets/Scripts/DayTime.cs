﻿using System;
using UnityEngine;

namespace Assets.Scripts
{
    abstract class DayTime
    {
        public static int GetDay()
        {
            return (int)DateTime.Now.DayOfWeek;
        }
        public static string GetTime()
        {
            return DateTime.Now.ToString($"T");
        }
        public static int[] TimeParser(string value)
        {
            string[] temp = value.Split(new char[] { ':' });
            return new int[]
            {
                Convert.ToInt32(temp[0]),
                Convert.ToInt32(temp[1]),
                Convert.ToInt32(temp[2])
            };
        }
        public static bool IsOneLess(string _one, string _two)
        {
            int[] one = TimeParser(_one);
            int[] two = TimeParser(_two);/*
            Debug.LogWarning("one " + one[0] + " " + one[1] + " " + one[2]);
            Debug.LogWarning("two " + two[0] + " " + two[1] + " " + two[2]);*/
            return new DateTime(2020, 1, 1, one[0], one[1], one[2]) < new DateTime(2020, 1, 1, two[0], two[1], two[2]);
        }
    }
}
