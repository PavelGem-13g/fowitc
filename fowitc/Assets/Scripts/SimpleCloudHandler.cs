﻿using UnityEngine.UI;
using UnityEngine;
using Vuforia;
using System.Collections;

public class SimpleCloudHandler : MonoBehaviour, IObjectRecoEventHandler
{
    private CloudRecoBehaviour mCloudRecoBehaviour;

    void Start()
    {
        mCloudRecoBehaviour = GetComponent<CloudRecoBehaviour>();

        if (mCloudRecoBehaviour)
        {
            //mCloudRecoBehaviour.RegisterEventHandler(this);
        }
        mCloudRecoBehaviour.CloudRecoEnabled = true;

    }

    public void OnInitialized(TargetFinder targetFinder)
    {
        Debug.Log("Инициализировано Cloud Reco");
    }
    public void OnInitError(TargetFinder.InitState initError)
    {
        Debug.Log("Ошибка инициализации Cloud Reco " + initError.ToString());
    }
    public void OnUpdateError(TargetFinder.UpdateState updateError)
    {
        Debug.Log("Ошибка обновления Cloud Reco " + updateError.ToString());
    }

    public void OnStateChanged(bool scanning)
    {

        if (scanning)
        {
            ObjectTracker tracker = TrackerManager.Instance.GetTracker<ObjectTracker>();
            tracker.GetTargetFinder<ImageTargetFinder>().ClearTrackables(false);
        }

    }

    public void OnNewSearchResult(TargetFinder.TargetSearchResult targetSearchResult)
    {
        TargetFinder.CloudRecoSearchResult cloudRecoSearchResult = (TargetFinder.CloudRecoSearchResult)targetSearchResult;

        string nameOfTarget = cloudRecoSearchResult.TargetName;
        string metadataOfTarget = cloudRecoSearchResult.MetaData;

        //Используйте эти данные, как вам нужно, например, создайте элемент UI Text и выведите на экран название распознанного объекта

    }
}